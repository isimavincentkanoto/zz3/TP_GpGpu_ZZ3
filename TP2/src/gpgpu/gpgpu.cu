#include <gpgpu.h>
#include <algorithm>
#include <iostream>
#include <vector>

__global__ void kernel_gray(float* device_image_float) {
	int32_t index = blockIdx.x * blockDim.x + threadIdx.x;
	device_image_float[index] = 1;
}

void getGPGPUInfo() {
	cudaDeviceProp cuda_properties;
	cudaGetDeviceProperties(&cuda_properties, 0);

	std::cout << cuda_properties.maxThreadsPerBlock;
}

float DivideBy255(uint8_t a) {
	
	float fa = a;
	return  fa / 255;
}

uint8_t MultiplyBy255(float a) {
	//std::cout << a * 255 << std::endl;
	return a * 255;
}

void GenerateGrayscaleImage(std::vector<uint8_t>& host_image_uint8, int32_t width, int32_t height){
	std::vector<float> host_image_float;
	host_image_float.reserve(host_image_uint8.size());

	/*for (int i = 0; i < host_image_uint8.size(); ++i) {
		host_image_float.push_back(host_image_uint8[i] / 255);
	}*/

	std::transform(host_image_uint8.begin(), host_image_uint8.end(),
		std::back_inserter(host_image_float),
		DivideBy255);


	float* device_image_float = nullptr;
	cudaMalloc(&device_image_float, host_image_uint8.size() * sizeof(float));
	cudaMemcpy(
		device_image_float,
		host_image_float.data(),
		host_image_uint8.size() * sizeof(float),
		cudaMemcpyHostToDevice);
	// Call to the kernel
	kernel_gray << <32 * 3, 1024 >> > (device_image_float);

	cudaMemcpy(
		host_image_float.data(),
		device_image_float,
		host_image_uint8.size() * sizeof(float),
		cudaMemcpyDeviceToHost);
	cudaFree(device_image_float);

	host_image_uint8.clear();
	std::transform(host_image_float.begin(), host_image_float.end(),
		std::back_inserter(host_image_uint8),
		MultiplyBy255);
}

